#include <iostream>
#include <cmath>

using namespace std;

int main() {

    int n;
    cin >> n;

    for (int i = 0; i < n; ++i) {
        string first, second;
        cin >> first >> second;
        cout << first << endl << second << endl;
        for (int j = 0; j < first.length(); ++j) {
            cout << (first[j] == second[j] ? "." : "*");
        }
        cout << endl << endl;
    }

    return 0;
}