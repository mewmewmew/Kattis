#include <iostream>
#include <cmath>

using namespace std;

int main() {

	string line;
	getline(cin, line);

	char lastChar = 0;
	for (auto c : line) {
		if (c == lastChar)
			continue;
		else {
			cout << c;
			lastChar = c;
		}
	}

	cout << endl;


	return 0;
}