#include <iostream>
#include <cmath>

using namespace std;

int main() {

	string line1, line2, result;
	getline(cin, line1);
	getline(cin, line2);

	line1 = string(line1.rbegin(), line1.rend());
	line2 = string(line2.rbegin(), line2.rend());

	int c;
	long minLen = min(line1.length(), line2.length());

	for (int i = 0; i < minLen; ++i) {
		int n, m;
		n = line1.at(i) - '0';
		m = line2.at(i) - '0';
		n += m + c;
		c = 0;
		if (n >= 10) {
			c = 1;
			n -= 10;
		}
		result = ((char) (n + '0')) + result;
	}

	string bigger = minLen == line1.length() ? line2 : line1;
	for (int j = minLen; j < bigger.length(); ++j) {
		int n;
		n = bigger.at(j) - '0';
		n += c;
		c = 0;
		if (n >= 10) {
			c = 1;
			n -= 10;
		}
		result = ((char) (n + '0')) + result;
	}
	if (c > 0) {
		result = '1' + result;
	}

	cout << result << endl;

	return 0;
}