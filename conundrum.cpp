#include <iostream>
#include <cmath>

using namespace std;

int main() {

    string c;
    cin >> c;

    int days = 0;

    for (int i = 0; i < c.length(); ++i) {
        if ((i % 3 == 0 && c[i] == 'P') || (i % 3 == 1 && c[i] == 'E') || (i % 3 == 2 && c[i] == 'R'))
            continue;
        days++;
    }

    cout << days << endl;

    return 0;
}