#include <iostream>
#include <cmath>

using namespace std;

int main() {

	int n, m;

	while (true) {
		cin >> n >> m;
		if (n == 0 && m == 0) {
			break;
		}

		if (n + m == 13) {
			cout << "Never speak again." << endl;
		} else if (n > m) {
			cout << "To the convention." << endl;
		} else if (n < m) {
			cout << "Left beehind." << endl;
		} else {
			cout << "Undecided." << endl;
		}
	}

	return 0;
}