#include <iostream>
#include <string>

using namespace std;

int main() {
    int x, n;
    cin >> x >> n;

    long use = 0;

    for (int i = 0; i < n; ++i) {
        int p;
        cin >> p;
        use += p;
    }

    cout << (n + 1) * x - use << endl;

    return 0;
}