#include <iostream>
#include <string>

using namespace std;

int main() {

	unsigned long n;

	cin >> n;

	unsigned long c = 2;

	for (int i = 1; i <= n; ++i) {
		c += c - 1;
	}

	cout << c * c << endl;

	return 0;
}