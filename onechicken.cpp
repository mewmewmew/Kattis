#include <iostream>
#include <cmath>

using namespace std;

int main() {

	int n, m;
	cin >> n >> m;

	n = m - n;
	if (n > 0) {
		cout << "Dr. Chaz will have " << n << " " << (n == 1 ? "piece" : "pieces") << " of chicken left over!" << endl;
	} else {
		cout << "Dr. Chaz needs " << (-n) << " more " << (n == -1 ? "piece" : "pieces") << " of chicken!" << endl;
	}

	return 0;
}