#include <iostream>
#include <cmath>

using namespace std;

int main() {

	int e, f, c;
	cin >> e >> f >> c;

	e += f;
	int d = 0;
	while (e >= c) {
		int q = e / c;
		d += q;
		e += q - q * c;
	}

	cout << d << endl;

	return 0;
}