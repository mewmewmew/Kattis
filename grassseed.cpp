#include <iostream>
#include <cmath>
#include <iomanip>

using namespace std;

int main() {

    double cost;
    int n;

    cin >> cost;
    cin >> n;

    double sum = 0;
    for (int i = 0; i < n; ++i) {
        double j, l;
        cin >> l >> j;
        sum += l * j;
    }

    cout << setprecision(12) << cost * sum << endl;

    return 0;
}