#include <iostream>
#include <cmath>
#include <map>

using namespace std;

int main() {

	string line;
	map<char, string> m = {{'A', ".-"},
	                       {'B', "-..."},
	                       {'C', "-.-."},
	                       {'D', "-.."},
	                       {'E', "."},
	                       {'F', "..-."},
	                       {'G', "--."},
	                       {'H', "...."},
	                       {'I', ".."},
	                       {'J', ".---"},
	                       {'K', "-.-"},
	                       {'L', ".-.."},
	                       {'M', "--"},
	                       {'N', "-."},
	                       {'O', "---"},
	                       {'P', ".--."},
	                       {'Q', "--.-"},
	                       {'R', ".-."},
	                       {'S', "..."},
	                       {'T', "-"},
	                       {'U', "..-"},
	                       {'V', "...-"},
	                       {'W', ".--"},
	                       {'X', "-..-"},
	                       {'Y', "-.--"},
	                       {'Z', "--.."},
	                       {'_', "..--"},
	                       {',', ".-.-"},
	                       {'.', "---."},
	                       {'?', "----"}};

	map<string, char> a = {{".-",   'A'},
	                       {"-...", 'B'},
	                       {"-.-.", 'C'},
	                       {"-..",  'D'},
	                       {".",    'E'},
	                       {"..-.", 'F'},
	                       {"--.",  'G'},
	                       {"....", 'H'},
	                       {"..",   'I'},
	                       {".---", 'J'},
	                       {"-.-",  'K'},
	                       {".-..", 'L'},
	                       {"--",   'M'},
	                       {"-.",   'N'},
	                       {"---",  'O'},
	                       {".--.", 'P'},
	                       {"--.-", 'Q'},
	                       {".-.",  'R'},
	                       {"...",  'S'},
	                       {"-",    'T'},
	                       {"..-",  'U'},
	                       {"...-", 'V'},
	                       {".--",  'W'},
	                       {"-..-", 'X'},
	                       {"-.--", 'Y'},
	                       {"--..", 'Z'},
	                       {"..--", '_'},
	                       {".-.-", ','},
	                       {"---.", '.'},
	                       {"----", '?'}};
	while (cin >> line) {

		string morseLine;
		string l = "";

		for (int i = 0; i < line.length(); ++i) {
			string morse = m[line[i]];
			l += to_string(morse.length());
			morseLine += morse;
		}

		l = string(l.rbegin(), l.rend());

		int pos = 0;
		for (int i = 0; i < line.length(); ++i) {
			string m = morseLine.substr(pos, l[i] - '0');
			pos += l[i] - '0';
			cout << a[m];
		}

		cout << endl;
	}

	return 0;
}