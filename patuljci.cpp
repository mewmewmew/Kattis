#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

int calcSum(vector<int> &items, unsigned long a, unsigned long b) {
	int sum = 0;
	for (unsigned long i = 0; i < items.size(); i++) {
		if (a == i || b == i)
			continue;
		sum += items.at(i);
	}
	return sum;
}

int main() {

	vector<int> items;
	for (int i = 0; i < 9; ++i) {
		int k;
		cin >> k;
		items.push_back(k);
	}

	unsigned long a, b;
	bool flag = false;

	for (a = 0; a < 9; ++a) {
		for (b = 0; b < 9; b++) {
			if (b == a) continue;

			if (calcSum(items, a, b) == 100) {
				flag = true;
				break;
			}
		}
		if (flag)
			break;
	}

	for (unsigned long i = 0; i < items.size(); i++) {
		if (a == i || b == i)
			continue;
		cout << items.at(i) << endl;
	}

	return 0;
}