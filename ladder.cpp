#include <iostream>
#include <cmath>

using namespace std;

int main() {

    int i, j;
    cin >> i >> j;
    double x = i / sin(M_PI * j / 180.0f);
    i = (int) x;
    if (x - i > 0) {
        i++;
    }
    
    cout << i << endl;

    return 0;
}