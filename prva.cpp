#include <iostream>
#include <cmath>
#include <vector>
#include <sstream>

using namespace std;

vector<string> split(const string &s, char delim) {
	stringstream ss(s);
	string item;
	vector<string> tokens;
	while (getline(ss, item, delim)) {
		tokens.push_back(item);
	}
	return tokens;
}

int main() {

	int n, m;
	cin >> n >> m;

	char board[n][m];

	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j) {
			cin >> board[i][j];
		}
	}

	string line;
	string minLine = "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz";

	for (int i = 0; i < n; ++i) {
		line = "";
		for (int j = 0; j < m; ++j) {
			line += board[i][j];
		}

		vector<string> lines = split(line, '#');
		for (auto line : lines) {
			if (line.length() >= 2)
				if (line < minLine)
					minLine = line;
		}
	}

	for (int j = 0; j < m; ++j) {
		line = "";
		for (int i = 0; i < n; ++i) {
			line += board[i][j];
		}

		vector<string> lines = split(line, '#');
		for (auto line : lines) {
			if (line.length() >= 2)
				if (line < minLine)
					minLine = line;
		}
	}

	cout << minLine << endl;

	return 0;
}