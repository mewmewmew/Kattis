#include <iostream>
#include <cmath>
#include <vector>
#include <limits>
#include <algorithm>

using namespace std;

int main() {

	int n;
	cin >> n;

	while (n != 0) {

		vector<int> l1, l1s, l2, a;

		for (int i = 0; i < n; ++i) {
			int k;
			cin >> k;
			l1.push_back(k);
			l1s.push_back(k);
		}

		for (int i = 0; i < n; ++i) {
			int k;
			cin >> k;
			l2.push_back(k);
		}

		sort(l1s.begin(), l1s.end());
		sort(l2.begin(), l2.end());

		for (int i = 0; i < l1.size(); ++i) {
			for (int j = 0; j < l1s.size(); ++j) {
				if (l1.at(i) == l1s.at(j)) {
					a.push_back(l2.at(j));
					break;
				}
			}
		}

		for (auto k : a) {
			cout << k << endl;
		}

		cin >> n;
		if (n != 0)
			cout << endl;
	}

	return 0;
}