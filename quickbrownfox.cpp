#include <iostream>
#include <cmath>
#include <algorithm>

using namespace std;

int main() {

	int n;
	string line;
	getline(cin, line);
	n = stoi(line);

	for (int i = 0; i < n; ++i) {

		int c = 0;
		int oc[26]{0};
		getline(cin, line);
		transform(line.begin(), line.end(), line.begin(), ::tolower);
		for (int j = 0; j < line.length(); ++j) {
			char ch = line[j];
			if (ch <= 'z' && ch >= 'a') {
				if (oc[ch - 'a'] == 0) {
					c++;
				}
				oc[ch - 'a']++;
			}
		}

		if (c == 26) {
			cout << "pangram" << endl;
		} else {
			cout << "missing ";
			for (int j = 0; j < 26; ++j) {
				if (oc[j] == 0) {
					cout << (char) (j + 'a');
				}
			}
			cout << endl;
		}

	}


	return 0;
}