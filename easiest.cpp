#include <iostream>
#include <cmath>

using namespace std;

int calc(int n) {
    int sum = 0;
    while (n != 0) {
        sum += n % 10;
        n /= 10;
    }
    return sum;
}

int main() {

    int n;
    cin >> n;

    while (n != 0) {
        int i = 11;
        int c = calc(n);
        while (calc(i * n) != c) {
            i++;
        }

        cout << i << endl;

        cin >> n;
    }

    return 0;
}