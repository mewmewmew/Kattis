#include <iostream>
#include <cmath>

using namespace std;

int main() {

	string n;
	cin >> n;

	int w = 0, b = 0;
	for (auto c : n) {
		if (c == 'W')
			w++;
		else b++;
	}

	if (w == b)
		cout << 1 << endl;
	else cout << 0 << endl;

	return 0;
}