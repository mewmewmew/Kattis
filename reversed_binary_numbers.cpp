#include <iostream>
#include <string>

using namespace std;

int main() {

	unsigned long n, m = 0;

	cin >> n;

	while (n != 0) {
		int i = n % 2;
		n = n / 2;
		m = m * 2 + i;
	}

	cout << m << endl;

	return 0;
}