#include <iostream>
#include <cmath>
#include <iomanip>

using namespace std;

int main() {

    double r, c;
    cin >> r >> c;

    double x = M_PI * pow(r - c, 2) * 100;
    double y = M_PI * pow(r, 2);

    cout << setprecision(12) << x / y << endl;


    return 0;
}