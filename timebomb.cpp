#include <iostream>
#include <cmath>
#include <map>

using namespace std;

int main() {

	string line;
	map<string, char> numbers;

	numbers["***\n* *\n* *\n* *\n***"] = '0';
	numbers["  *\n  *\n  *\n  *\n  *"] = '1';
	numbers["***\n  *\n***\n*  \n***"] = '2';
	numbers["***\n  *\n***\n  *\n***"] = '3';
	numbers["* *\n* *\n***\n  *\n  *"] = '4';
	numbers["***\n*  \n***\n  *\n***"] = '5';
	numbers["***\n*  \n***\n* *\n***"] = '6';
	numbers["***\n  *\n  *\n  *\n  *"] = '7';
	numbers["***\n* *\n***\n* *\n***"] = '8';
	numbers["***\n* *\n***\n  *\n***"] = '9';

	getline(cin, line);

	unsigned long colCount = line.length();
	char rows[5][colCount]{0};

	for (int i = 0; i < 5; ++i) {
		for (int j = 0; j < colCount; ++j) {
			rows[i][j] = line[j];
		}
		if (i < 4)
			getline(cin, line);
	}

	cout << endl;

	string numberSeq;
	int pos = 0;
	while (pos < colCount) {
		string number;
		for (int i = 0; i < 5; ++i) {
			for (int j = pos; j < pos + 3; ++j) {
				number += rows[i][j];
			}
			if (i < 4)
				number += "\n";
		}

		pos += 4;

		auto f = numbers.find(number);
		if (f != numbers.end()) {
			numberSeq += f->second;
		} else {
			cout << "BOOM!!" << endl;
			return 0;
		}
	}

	long x = stol(numberSeq);
	if (x % 6 == 0) {
		cout << "BEER!!" << endl;
	} else {
		cout << "BOOM!!" << endl;
	}

	return 0;
}

//***   * *** *** * * *** *** *** *** ***
//* *   *   *   * * * *   *     * * * * *
//* *   * *** *** *** *** ***   * *** ***
//* *   * *     *   *   * * *   * * *   *
//***   * *** ***   * *** ***   * *** ***