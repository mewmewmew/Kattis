#include <iostream>
#include <cmath>

using namespace std;

int reverse(int n) {
    int m = 0;

    while (n != 0) {
        m = m * 10 + n % 10;
        n /= 10;
    }

    return m;
}

int main() {

    int n, m;

    cin >> n >> m;

    n = reverse(n);
    m = reverse(m);

    cout << (m > n ? m : n) << endl;

    return 0;
}