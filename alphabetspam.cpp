#include <iostream>
#include <cmath>
#include <iomanip>

using namespace std;

int main() {

	string line;
	getline(cin, line);

	double w = 0, l = 0, u = 0, s = 0;

	for (int i = 0; i < line.length(); ++i) {
		if (line[i] == '_')
			w++;
		else if (line[i] >= 'a' && line[i] <= 'z') {
			l++;
		} else if (line[i] >= 'A' && line[i] <= 'Z') {
			u++;
		} else {
			s++;
		}
	}

	cout << fixed << std::setprecision(7);
	cout << w / (double) line.length() << endl;
	cout << l / (double) line.length() << endl;
	cout << u / (double) line.length() << endl;
	cout << s / (double) line.length() << endl;


	return 0;
}