#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;

int main() {

	int n;
	cin >> n;

	vector<string> names;
	vector<string> copyNames;

	for (int i = 0; i < n; ++i) {
		string l;
		cin >> l;
		names.push_back(l);
		copyNames.push_back(l);
	}

	sort(copyNames.begin(), copyNames.end());

	bool flag = false;

	for (int i = 0; i < n; ++i) {
		if (copyNames.at(i) != names.at(i)) {
			flag = true;
			break;
		}
	}

	if (!flag) {
		cout << "INCREASING" << endl;
		return 0;
	}

	flag = false;

	for (int i = 0; i < n; ++i) {
		if (copyNames.at(i) != names.at(n - i - 1)) {
			flag = true;
			break;
		}
	}

	if (!flag) {
		cout << "DECREASING" << endl;
		return 0;
	}

	cout << "NEITHER" << endl;

	return 0;
}