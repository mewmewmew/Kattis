#include <iostream>
#include <cmath>

using namespace std;

int main() {

	long n;
	cin >> n;

	for (long i = 0; i < n; ++i) {
		string line;
		cin >> line;
		long len = line.length();
		long sq = static_cast<long>(sqrt(len));

		char board[sq][sq];

		for (long j = 0; j < sq; ++j) {
			for (long k = 0; k < sq; ++k) {
				board[j][k] = line[j * sq + k];
			}
		}

		for (long j = sq - 1; j >= 0; --j) {
			for (long k = 0; k < sq; ++k) {
				cout << board[k][j];
			}
		}

		cout << endl;
	}


	return 0;
}