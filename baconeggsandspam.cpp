#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <map>
#include <sstream>

using namespace std;


vector<string> split(const string &s, char delim) {
	stringstream ss(s);
	string item;
	vector<string> tokens;
	while (getline(ss, item, delim)) {
		tokens.push_back(item);
	}
	return tokens;
}

int main() {

	int n;
	cin >> n;
	while (n != 0) {
		map<string, vector<string>> x;
		string line;
		getline(cin, line);
		for (int i = 0; i < n; ++i) {
			getline(cin, line);
			auto data = split(line, ' ');
			for (int j = 1; j < data.size(); ++j) {
				x[data.at(j)].push_back(data.at(0));
			}
		}

		for (auto y : x) {
			cout << y.first << " ";
			sort(y.second.begin(), y.second.end());
			for (int i = 0; i < y.second.size(); ++i) {
				if (i == y.second.size() - 1) {
					cout << y.second.at(i) << endl;
				} else {
					cout << y.second.at(i) << " ";
				}
			}
		}

		cout << endl;
		cin >> n;
	}
	return 0;
}