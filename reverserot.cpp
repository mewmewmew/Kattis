#include <iostream>
#include <cmath>
#include <map>

using namespace std;

int main() {

    map<char, int> chars;
    map<int, char> rchars;
    for (int i = 0; i < 26; ++i) {
        chars['A' + i] = i;
        rchars[i] = (char) ('A' + i);
    }

    chars['_'] = 26;
    rchars[26] = '_';

    chars['.'] = 27;
    rchars[27] = '.';


    while (true) {
        int n;
        cin >> n;
        if (n == 0)
            break;

        string line;
        cin >> line;

        line = string(line.rbegin(), line.rend());

        for (int i = 0; i < line.length(); ++i) {
            line[i] = rchars[(chars[line[i]] + n) % 28];
        }

        cout << line << endl;
    }

    return 0;
}