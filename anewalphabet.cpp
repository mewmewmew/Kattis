#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

int main() {

	string line;
	getline(cin, line);


	vector<string> instedOf(
			{"@", "8", "(", "|)", "3", "#",
			 "6", "[-]", "|", "_|", "|<", "1",
			 "[]\\/[]", "[]\\[]", "0", "|D", "(,)", "|Z",
			 "$", "']['", "|_|", "\\/", "\\/\\/", "}{", "`/", "2"});

	string result;

	for (int i = 0; i < line.length(); ++i) {
		if (line[i] >= 'a' && line[i] <= 'z') {
			result += instedOf[line[i] - 'a'];
		} else if (line[i] >= 'A' && line[i] <= 'Z') {
			result += instedOf[line[i] - 'A'];
		} else {
			result += line[i];
		}
	}

	cout << result << endl;

	return 0;
}