#include <iostream>
#include <cmath>

using namespace std;

int main() {

    int n;

    cin >> n;

    double g = 9.81;

    for (int i = 0; i < n; ++i) {

        double v0, t, x1, h1, h2;
        cin >> v0 >> t >> x1 >> h1 >> h2;

        h1++;
        h2--;

        double s, c;
        sincos(t * M_PI / 180, &s, &c);

        double tm = x1 / (v0 * c);
        double y = v0 * tm * s - 0.5 * g * tm * tm;

        if (y > h1 && y < h2) {
            cout << "Safe" << endl;
        } else {
            cout << "Not Safe" << endl;
        }

    }

    return 0;
}