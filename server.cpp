#include <iostream>
#include <cmath>

using namespace std;

int main() {

    int n, t;
    cin >> n >> t;

    int count = 0;

    for (int i = 0; i < n; ++i) {
        int k;
        cin >> k;
        t -= k;
        if (t >= 0)
            count++;
        else break;
    }

    cout << count << endl;

    return 0;
}