#include <iostream>
#include <string>
using namespace std;

int main() {

    string line;
    getline(cin,line);
    for (int i = 0; i < line.length(); ++i) {
        if (line[i] >= 'A' && line[i] <= 'Z')
            cout << line[i];
    }

    cout << endl;

    return 0;
}