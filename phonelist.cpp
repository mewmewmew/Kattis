#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;

int main() {

	int n;
	cin >> n;

	for (int i = 0; i < n; ++i) {

		int t;
		cin >> t;

		vector<string> phone;

		for (int j = 0; j < t; ++j) {
			string s;
			cin >> s;
			phone.push_back(s);
		}

		sort(phone.begin(), phone.end());
		bool con = true;
		for (int j = 0; j < t - 1; ++j) {
			if (phone[j].length() < phone[j + 1].length()) {
				if (phone[j + 1].substr(0, phone[j].length()) == phone[j]) {
					con = false;
					break;
				}
			}
		}

		cout << (con ? "YES" : "NO") << endl;
	}

	return 0;
}