#include <iostream>
#include <cmath>
#include <iomanip>

using namespace std;

int main() {

    long double n;
    cin >> n;

    cout << setprecision(6);

    cout << (M_PI * n * n) << endl;

    cout << (n + n) * (n + n) / 2 << endl;


    return 0;
}