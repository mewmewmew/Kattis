#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;

int main() {

	int n;
	cin >> n;

	char b;
	cin >> b;

	vector<int> dominant({11, 4, 3, 20, 10, 14, 0, 0});
	vector<int> not_dominant({11, 4, 3, 2, 10, 0, 0, 0});
	vector<char> items({'A', 'K', 'Q', 'J', 'T', '9', '8', '7'});
	int sum = 0;
	for (int i = 0; i < n * 4; ++i) {
		string card;
		cin >> card;
		auto idx = find(items.begin(), items.end(), card[0]);
		if (card[1] == b) {
			sum += dominant.at(idx - items.begin());
		} else {
			sum += not_dominant.at(idx - items.begin());
		}
	}

	cout << sum << endl;


	return 0;
}