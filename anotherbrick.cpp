#include <iostream>
#include <cmath>

using namespace std;

int main() {

	int h, w, n;
	cin >> h >> w >> n;
	int x[n];

	int curr = 0;

	for (int i = 0; i < n; ++i) {
		cin >> x[i];
		curr += x[i];
		if (curr == w) {
			h--;
			curr = 0;
		} else if (curr > w) {
			cout << "NO" << endl;
			return 0;
		}

		if (h == 0) {
			cout << "YES" << endl;
			return 0;
		}
	}

	cout << "NO" << endl;

	return 0;
}