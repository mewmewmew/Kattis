// NOT WORK

#include <iostream>
#include <cmath>

using namespace std;

inline int cardNameToIndex(char c) {
	switch (c) {
		case 'P':
			return 0;
		case 'K':
			return 1;
		case 'H':
			return 2;
		case 'T':
			return 3;
	}
}

int main() {

	int cards[4][13]{0};

	int n;
	char c;

//	while (cin >> c) {
//		cin >> n;
//
//		cards[cardNameToIndex(c)][n]++;
//		if (cards[cardNameToIndex(c)][n] > 1) {
//			cout << "GRESKA" << endl;
//			return 0;
//		}
//	}

	string line;
	getline(cin, line);
	for (int i = 0; i < line.length(); i += 3) {
		c = line[i];
		n = (line[i + 1] - '0') * 10 + (line[i + 2] - '0');
		cards[cardNameToIndex(c)][n]++;
		if (cards[cardNameToIndex(c)][n] > 1) {
			cout << "GRESKA" << endl;
			return 0;
		}
	}

	if (line.length() % 3 != 0) {
		cout << "GRESKA" << endl;
		return 0;
	}

	for (int i = 0; i < 4; ++i) {
		int k = 13;
		for (int j = 0; j < 13; ++j) {
			if (cards[i][j] != 0)
				k--;
		}
		if (i == 3)
			cout << k << endl;
		else
			cout << k << " ";
	}

	return 0;
}