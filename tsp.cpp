#include <iostream>
#include <cmath>
#include <vector>

double dist(int i, int j);

using namespace std;

vector<pair<double, double >> points;

int main() {

	int n;
	cin >> n;

	for (int i = 0; i < n; ++i) {

		double x, y;
		cin >> x >> y;
		points.emplace_back(x, y);

	}

	bool used[n]{false};
	vector<int> tour(n, 0);

	used[0] = true;
	tour[0] = 0;
	int best;
	for (int j = 1; j < n; ++j) {
		best = -1;
		for (int k = 0; k < n; ++k) {
			if (!used[k] && (best == -1 || (dist(tour[j - 1], k) < dist(tour[j - 1], best)))) {
				best = k;
			}
		}
		tour[j] = best;
		used[best] = true;
	}

	for (auto t : tour) {
		cout << t << endl;
	}

	return 0;
}

double dist(int i, int j) {
	return pow(points[i].first - points[j].first, 2) + pow(points[i].second - points[j].second, 2);
}