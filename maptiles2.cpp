#include <iostream>
#include <cmath>

using namespace std;

int main() {

	string n;
	cin >> n;

	cout << n.length() << " ";
	unsigned long long scale = static_cast<unsigned long long int>(pow(2, n.length() - 1));
	unsigned long long x = 0, y = 0;
	unsigned long long maxX = scale - 1, maxY = scale - 1;

	for (int i = 0; i < n.length(); ++i) {
		if (n[i] == '0') {
			maxX -= scale;
			maxY -= scale;
		} else if (n[i] == '1') {
			x += scale;
			maxY -= scale;
		} else if (n[i] == '2') {
			maxX -= scale;
			y += scale;
		} else if (n[i] == '3') {
			x += scale;
			y += scale;
		}
		scale /= 2;
	}

	cout << x << " " << y << endl;

	return 0;
}