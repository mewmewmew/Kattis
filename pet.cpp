#include <iostream>
#include <string>

using namespace std;

int main() {

	int max = 0;
	int index = 0;

	for (int i = 1; i <= 5; ++i) {

		int n, sum = 0;
		for (int j = 0; j < 4; ++j) {
			cin >> n;
			sum += n;
		}

		if (sum > max) {
			max = sum;
			index = i;
		}
	}

	cout << index << " " << max << endl;

	return 0;
}