#include <iostream>
#include <cmath>
#include <climits>
#include <iomanip>

using namespace std;

int main() {

	int p, a, b, c, d, n;

	cin >> p >> a >> b >> c >> d >> n;

	double maxDecline = 0;
	double prevPrice = -1;
	for (int i = 1; i <= n; ++i) {
		double price = p * (sin((a * i + b)) + cos((c * i + d)) + 2);
		if (prevPrice == -1) {
			prevPrice = price;
			continue;
		}

		if (prevPrice < price) {
			prevPrice = price;
		} else if (prevPrice - price > maxDecline) {
			maxDecline = prevPrice - price;
		}
	}

	cout << setprecision(12);

	cout << maxDecline << endl;

	return 0;
}