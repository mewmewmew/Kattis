#include <iostream>
#include <cmath>
#include <map>

using namespace std;

int main() {

	int n;
	cin >> n;
	for (int i = 1; i <= n; i++) {
		int g;
		cin >> g;
		map<unsigned long long, int> c;
		for (int j = 0; j < g; ++j) {
			unsigned long long ac;
			cin >> ac;
			auto f = c.find(ac);
			if (f == c.end())
				c[ac] = 1;
			else c[ac]++;
		}

		cout << "Case #" << i << ": ";

		for (auto f : c) {
			if (f.second == 1) {
				cout << f.first << endl;
				break;
			}
		}
	}


	return 0;
}